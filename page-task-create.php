<?php if ($_SESSION['user']['role'] == 1) : ?>


    <form id="FormTaskCreate" action="/?page=task-create" method="post" class="form-task">
        <div class="row">
            <div class="col-12">
                <h2 class="h1">Добавление нового задания</h2>
            </div>
        </div>
        <div class="row mt-1">
            <label for="TextareaFormTaskCreateText" class="col-md-2">Текст задания</label>
            <div class="col-md-10">
                <textarea class="form-control" id="TextareaFormTaskCreateText" name="text" cols="30" rows="2" required></textarea>
            </div>
        </div>
        <div class="row mt-1">
            <label for="TextareaFormTaskCreateText" class="col-md-2">Решение</label>
            <div class="col-md-10">
                <textarea class="form-control" id="TextareaFormTaskCreateText" name="desc" cols="30" rows="2" required></textarea>
            </div>
        </div>
        <div class="row mt-1">
            <label for="InputFormTaskCreateAnswer" class="col-md-2 col-form-label">Правильный ответ</label>
            <div class="col-md-10">
                <input type="text" class="form-control" name="answer" id="InputFormTaskCreateAnswer" required>
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-12 col-md-3 row">
                <div class="col-md-2">
                    <h5>xA<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xa1" value="<?= $task->xa1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="ya1" value="<?= $task->ya1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="za1" value="<?= $task->za1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 row">
                <div class="col-md-2">
                    <h5>xB<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xb1" value="<?= $task->xb1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yb1" value="<?= $task->yb1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zb1" value="<?= $task->zb1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 row">
                <div class="col-md-2">
                    <h5>xC<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xc1" value="<?= $task->xc1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yc1" value="<?= $task->yc1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zc1" value="<?= $task->zc1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 row">
                <div class="col-md-2">
                    <h5>xC<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xc2" value="<?= $task->xc2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yc2" value="<?= $task->yc2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zc2" value="<?= $task->zc2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 row">
                <div class="col-md-2">
                    <h5>xD<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xd1" value="<?= $task->xd1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yd1" value="<?= $task->yd1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zd1" value="<?= $task->zd1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 row">
                <div class="col-md-2">
                    <h5>xA<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xa2" value="<?= $task->xa2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="ya2" value="<?= $task->ya2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="za2" value="<?= $task->za2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 row">
                <div class="col-md-2">
                    <h5>xB<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xb2" value="<?= $task->xb2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yb2" value="<?= $task->yb2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zb2" value="<?= $task->zb2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>


        </div>
        <div class="row mt-3 mb-3">
            <div class="col-12 d-flex justify-content-center align-items-center">
                <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-3 d-flex flex-column">
                <h5>Прямая L1L2</h5>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>L1</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xl1" value="<?= $task->xl1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yl1" value="<?= $task->yl1 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zl1" value="<?= $task->zl1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>L2</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xl2" value="<?= $task->xl2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yl2" value="<?= $task->yl2 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zl2" value="<?= $task->zl2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 d-flex flex-column">
                <h5>Прямая N1N2</h5>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>N1</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xn1" value="<?= $task->xn1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yn1" value="<?= $task->yn1 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zn1" value="<?= $task->zn1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>N2</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xn2" value="<?= $task->xn2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yn2" value="<?= $task->yn2 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zn2" value="<?= $task->zn2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 d-flex flex-column">
                <h5>Прямая M1M2</h5>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>M1</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xm1" value="<?= $task->xm1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="ym1" value="<?= $task->ym1 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zm1" value="<?= $task->zm1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>M2</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xm2" value="<?= $task->xm2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="ym2" value="<?= $task->ym2 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zm2" value="<?= $task->zm2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 d-flex flex-column">
                <h5>Прямая K1K2</h5>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>K1</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xk1" value="<?= $task->xk1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yk1" value="<?= $task->yk1 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zk1" value="<?= $task->zk1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>K2</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xk2" value="<?= $task->xk2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yk2" value="<?= $task->yk2 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zk2" value="<?= $task->zk2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 d-flex flex-column">
                <h5>Прямая J1J2</h5>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>J1</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xj1" value="<?= $task->xj1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yj1" value="<?= $task->yj1 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zj1" value="<?= $task->zj1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
                <div class="d-flex">
                    <p class="m-1 mr-3"><strong>J2</strong></p>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="xj2" value="<?= $task->xj2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="yj2" value="<?= $task->yj2 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" value="0" name="zj2" value="<?= $task->zj2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9 d-flex justify-content-center align-items-end">
                <input type="hidden" name="user" value="<?= $_SESSION['user']['id'] ?>">
                <div class="btn-group">
                    <button type="button" id="ButtonResetDefault" name="form-task-create" class="btn btn-outline-green">По умолчанию</button>
                    <button type="reset" name="form-task-create" class="btn btn-outline-green">Сбросить</button>
                    <button type="submit" name="form-task-create" class="btn btn-green text-white">Создать задание</button>
                </div>

            </div>
        </div>
        </div>

    </form>
<?php else : ?>
    <div class="row site-style-default pt-3 pb-3">
        <div class="col-md-4 d-flex justify-content-center align-items-center">
            <img src="assets/img/icon-forbidden.png" style="height: 250px;" alt="">
        </div>
        <div class="col-md-8 d-flex flex-column justify-content-center align-items-center">
            <h3 class="display-3 m-3 text-center">
                Доступ запрещен
            </h3>
            <p class="mt-1">
                Добавлять новые задания может только преподаватель
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <a href="/" class="btn btn-lg btn-primary m-3 p-3">На главную</a>
        </div>
    </div>
<?php endif ?>