<?php

global $db;

$groups = $db->getGroups();
$roles = $db->getRoles();

?>

<div class="modal fade" id="ModalRegistration" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="ModalRegistrationLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalRegistrationLabel">Регистрация нового пользователя</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="FormRegistration" action="/" method="POST">
                    <div class="form-group">
                        <label for="InputRegistrationEmail">Адрес электронной почты</label>
                        <input type="email" name="email" class="form-control" id="InputRegistrationEmail" placeholder="name@mail.ru" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="InputRegistrationPassword">Пароль</label>
                            <input type="password" name="password" class="form-control" id="InputRegistrationPassword" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="InputRegistrationPasswordRepeat">Повторите пароль</label>
                            <input type="password" class="form-control" id="InputRegistrationPasswordRepeat" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName">Полное имя</label>
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Иванов Иван Иванович" required>
                    </div>
                    <div class="form-row">
                        <div class="col-12 col-md-6">
                            <label for="SelectGroup">Ваша учебная группа</label>
                            <select id="SelectGroup" class="custom-select" name="group" required>
                                <?php foreach ($groups as $key => $group) : ?>
                                    <?php if ($key == 0) : ?>
                                        <option selected value="<?= $group->id ?>"><?= $group->name ?></option>
                                    <?php else : ?>
                                        <option value="<?= $group->id ?>"><?= $group->name ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label>Ваша роль</label>
                            <?php foreach ($roles as $key => $role) : ?>
                                <div class="form-check ml-3">
                                    <?php if ($key == 0) : ?>
                                        <input class="form-check-input" type="radio" name="role" id="RadioRole<?= $role->id ?>" value="<?= $role->id ?>" checked>
                                        <label class="form-check-label" for="RadioRole<?= $role->id ?>">
                                            <?= $role->name ?>
                                        </label>
                                    <?php else : ?>
                                        <input class="form-check-input" type="radio" name="role" id="RadioRole<?= $role->id ?>" value="<?= $role->id ?>">
                                        <label class="form-check-label" for="RadioRole<?= $role->id ?>">
                                            <?= $role->name ?>
                                        </label>
                                    <?php endif ?>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Отмена</button>
                <button type="submit" name="form-registration" form="FormRegistration" class="btn btn-primary text-white">Далее</button>
            </div>
        </div>
    </div>
</div>