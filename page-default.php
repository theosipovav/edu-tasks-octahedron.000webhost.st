<?php
global $db;
?>
<div class="row mt-2 mb-2">
    <div class="col-12 text-center">
        <h1 class="display-1">Добро пожаловать!</h1>
    </div>
</div>
<?php if ($isLogged) : ?>
    <div class="row">
        <div class="col-12">
            <div class="card mt-3 mb-1">
                <div class="card-body">
                    <h5 class="card-title">Статистика</h5>
                    <div class="d-flex flex-column">
                        <?php if ($_SESSION['user']['role'] == 1) : ?>
                            <p class="card-text">Вами опубликовано <?= count($db->getTasksByAuthors([$_SESSION['user']['id']])) ?> задач</p>
                        <?php else : ?>
                            <p class="card-text">Вам доступно <?= count(getTaskForGroup($_SESSION['user']['group'])) ?> задач</p>
                            <p class="card-text">Вы решили <?= count($db->getAnswersByUser($_SESSION['user']['id'])) ?> задач</p>
                        <?php endif ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex justify-content-center align-items-center m-3">
            <img src="assets/img/a.png" class="img-a">

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p>
                <b>k-Смежностный многогранник</b> — это выпуклый многогранник, в котором любое k-элементное подмножество его вершин является множеством вершин некоторой грани этого многогранника.
            </p>
            <p>
                <b>Октаэдр</b> (греч. οκτάεδρον от οκτώ «восемь» + έδρα «основание») — многогранник с восемью гранями.
            </p>
            <p>
                Пра́вильный окта́эдр является одним из пяти выпуклых правильных многогранников[1], так называемых платоновых тел; его грани — восемь равносторонних треугольников.
            </p>
            <ul>Правильный октаэдр
                <li>двойственен кубу;</li>
                <li>полное усечение тетраэдра;</li>
                <li>квадратная бипирамида в любом из трёх ортогональных направлений;</li>
                <li>треугольная антипризма в любом из четырёх направлений;</li>
                <li>трёхмерный шар в метрике городских кварталов;</li>
            </ul>
            <p>
                Октаэдр — трёхмерный вариант более общего понятия гипероктаэдр.
            </p>
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <p class="lead text-center m-3">Уважаемый пользователь, для неавторизованных пользователь функционал сайта недоступен</p>
                <div class="card-body d-flex flex-column justify-content-center align-items-center">
                    <p class="lead">
                        <span>Пожалуйста, </span>
                        <button type="button" class="btn btn-primary text-white" data-bs-toggle="modal" data-bs-target="#ModalLogIn">
                            войдите
                        </button>
                        <span>на сайт под своими учетными данными или пройдите</span>
                        <button type="button" class="btn btn-primary text-white" data-bs-toggle="modal" data-bs-target="#ModalRegistration">
                            регистрацию
                        </button>
                        <span>на сайте</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>