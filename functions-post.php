<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    /**
     * Регистрация нового пользователя
     */
    if (isset($_POST['form-registration'])) {
        reg($_POST);
    }
    /**
     * Авторизация
     */
    if (isset($_POST['form-login'])) {
        login($_POST);
    }
    /**
     * Выход
     */
    if (isset($_POST['form-logout'])) {
        session_unset();
        $isLogged = false;
    }
    /**
     * Обновление профиля
     */
    if (isset($_POST['form-profile-update'])) {
        updateProfile($_POST);
    }
    /**
     * Добавить ответ
     */
    if (isset($_POST['form-answer-create'])) {
        createAnswer($_POST);
    }
    /**
     * Добавить ответ
     */
    if (isset($_POST['form-answer-update'])) {
        updateAnswer($_POST);
    }
    /**
     * Добавить задание
     */
    if (isset($_POST['form-task-create'])) {
        createTask($_POST);
    }
    /**
     * Удалить задание
     */
    if (isset($_POST['form-task-remove'])) {
        removeTask($_POST['id']);
    }
}
