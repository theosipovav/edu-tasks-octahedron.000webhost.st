<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Учебные задания по теме "Группы самосовмещений правильных многоугольников"</title>
    <link rel="canonical" href="." />
    <link href="assets/lib/fontawesome-free-5.15.1-web/css/all.min.css" rel="stylesheet" />
    <link href="assets/lib/bootstrap-5_0_0-beta1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <link color="#e52037" rel="mask-icon" href="assets/img/safari-pinned-tab.svg">
</head>