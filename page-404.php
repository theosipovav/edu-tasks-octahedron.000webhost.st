<div class="jumbotron jumbotron-fluid bg-warning site-style-default">
    <div class="container">
        <div class="d-flex flex-column">

            <h1 class="g1">ERROR 404</h1>
            <p class="lead">Страница не найдена или больше не существует</p>
            <hr>
            <div class="d-flex justify-content-center">
                <a href="/" class="btn btn-lg btn-primary text-white">Вернуться на главную</a>
            </div>

        </div>
    </div>
</div>