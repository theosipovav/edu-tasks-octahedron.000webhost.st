<?php
global $db;
$answers = $db->getAnswers();
$filterUsers = getUsersInAnswers();


if (isset($_POST['form-filter'])) {

    // Фильтр по пользователям
    if (isset($_POST['user']) && $_POST['user'] != "") {
        $answersFilter = [];
        foreach ($answers as $key => $answer) {
            if ($_POST['user'] == $answer->user) {
                $answersFilter[] = $answer;
            }
        }
        $answers = $answersFilter;
    }


    // Фильтр по статусу
    if (isset($_POST['status']) && $_POST['status'] != "") {
        $answersFilter = [];
        foreach ($answers as $key => $answer) {
            if (checkAnswer($answer->id, $answer->task)) {
                if ($_POST['status'] == 1) {
                    $answersFilter[] = $answer;
                }
            } else {
                if ($_POST['status'] == 2) {
                    $answersFilter[] = $answer;
                }
            }
        }
        $answers = $answersFilter;
    }
}

?>

<div class="row">
    <div class="col-12">
        <h2 class="display-3">Результаты выполнения заданий</h2>
    </div>
    <div class="col-12">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Номер задачи</th>
                    <th scope="col">Студент</th>
                    <th scope="col">Дата ответа</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Фильтр</td>
                    <td colspan="2">
                        <form action="/?page=answers" id="FormTaskFilter" method="post">
                            <div class="d-flex">
                                <div class="d-flex flex-column flex-grow-1 pr-1 pl-1 mr-1 ml-1">
                                    <label for="SelectFormTaskFilterUser">Студент</label>
                                    <select name="user" id="SelectFormTaskFilterUser" class="form-control">
                                        <option disabled selected>Студент</option>
                                        <?php foreach ($filterUsers as $key => $user) : ?>
                                            <option value="<?= $user->id ?>"><?= $user->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="d-flex flex-column flex-grow-1 pr-1 pl-1 mr-1 ml-1">
                                    <label for="SelectFormTaskFilterUser">Статус</label>
                                    <select name="status" id="SelectFormTaskFilterUser" class="form-control">
                                        <option disabled selected>Статус</option>
                                        <option value="1">Решено верно</option>
                                        <option value="2">Решено с ошибкой</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </td>
                    <td class="d-flex justify-content-center align-items-center">
                        <div class="btn-group-vertical" style="max-width: 150px;">
                            <button type="submit" form="FormTaskFilter" name="form-filter" class="btn btn-secondary">Применить</button>
                            <a href="/?page=answers" class="btn btn-secondary">Показать все</a>
                        </div>
                    </td>
                </tr>
                <?php foreach ($answers as $key => $answer) : ?>
                    <?php if (checkAnswer($answer->id, $answer->task)) : ?>
                        <tr onclick="document.location = '/?page=task&id=<?= $answer->task ?>';" class="tr-stretched  bg-success text-light">
                            <th><?= $key ?></th>
                            <td><strong><?= $answer->id ?></strong></td>
                            <td><?= $db->getUserById($answer->user)->name ?></td>
                            <td><?= $answer->update_dt ?></td>
                        </tr>

                    <?php else : ?>
                        <tr onclick="document.location = '/?page=task&id=<?= $answer->task ?>';" class="tr-stretched  bg-danger text-light">
                            <th><?= $key ?></th>
                            <td><strong><?= $answer->id ?></strong></td>
                            <td><?= $db->getUserById($answer->user)->name ?></td>
                            <td><?= $answer->update_dt ?></td>
                        </tr>

                    <?php endif ?>
                <?php endforeach ?>

            </tbody>
        </table>
    </div>
</div>