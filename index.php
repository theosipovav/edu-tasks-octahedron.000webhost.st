<?php
session_start();
?>
<?php include_once 'db.php' ?>
<?php include_once 'functions.php' ?>
<?php include_once 'functions-post.php' ?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'head.php'; ?>

<body>
    <?php
    include_once 'header.php';
    include_once 'page.php';
    include_once 'footer.php';
    ?>
</body>

</html>