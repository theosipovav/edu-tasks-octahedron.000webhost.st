$(document).ready(function () {
    $("#FormLogIn .link-of-login-in-reg").click(function (e) {
        e.preventDefault();
        $('#ModalLogIn').modal('hide');
        $('#ModalRegistration').modal('show');
    });
    /**
     * Проверка паролей при регистрации
     */
    $("#FormRegistration").submit(function (e) {
        var p1 = $("#InputRegistrationPassword").val();
        var p2 = $("#InputRegistrationPasswordRepeat").val();
        if (p1 != p2) {
            isValid = false;
            $("#InputRegistrationPassword").addClass("is-invalid");
            $("#InputRegistrationPasswordRepeat").addClass("is-invalid");
            alert("Введенные пароли не совпадают");
            e.preventDefault();
        }
    });
    /**
     * Подтверждение действия
     */
    $(".btn-confirm").click(function (e) {
        if (confirm("Вы подтверждаете операцию?")) {
        } else {
            e.preventDefault();
        }
    });
    /**
     * Поиск элемента содержащие другие данных точек
     */
    var formTask = $(".form-task");
    if (formTask.length > 0) {
        if ($("#canvas").length > 0) {
            renderOctahedron();
        }
    }
    /**
     * Динамическая отрисовка графика при изменений параметров
     */
    $(".form-task input").change(function (e) {
        renderOctahedron();
    });


    $("#ButtonResetDefault").click(function (e) {

        /*
        Найдите радиус сферы, вписанной в единичный октаэдр.

        Радиус сферы равен радиусу окружности, вписанной в ромб 
        */


        $("textarea[name=text]").val("Найдите радиус сферы, вписанной в единичный октаэдр.");


        $("textarea[name=desc]").val("Радиус сферы равен радиусу окружности, вписанной в ромб A2J1B2J2, в котором A2=J1=√3/2, J1J2=1, A20=√2/2. Тогда высота ромба опущена из вершины J1, будет равна √6/3");

        $("input[name=answer]").val("√6/3");

        $("input[name=xa1]").val("-2");
        $("input[name=ya1]").val("0");
        $("input[name=za1]").val("-2");

        $("input[name=xb1]").val("2");
        $("input[name=yb1]").val("0");
        $("input[name=zb1]").val("-2");

        $("input[name=xc1]").val("2");
        $("input[name=yc1]").val("0");
        $("input[name=zc1]").val("2");

        $("input[name=xd1]").val("-2");
        $("input[name=yd1]").val("0");
        $("input[name=zd1]").val("2");


        $("input[name=xa2]").val("0");
        $("input[name=ya2]").val("4");
        $("input[name=za2]").val("0");

        $("input[name=xb2]").val("0");
        $("input[name=yb2]").val("-4");
        $("input[name=zb2]").val("0");

        $("input[name=xl1]").val("-2");
        $("input[name=yl1]").val("0");
        $("input[name=zl1]").val("0");
        $("input[name=xl2]").val("0");
        $("input[name=yl2]").val("4");
        $("input[name=zl2]").val("0");

        $("input[name=xn1]").val("-2");
        $("input[name=yn1]").val("0");
        $("input[name=zn1]").val("0");
        $("input[name=xn2]").val("0");
        $("input[name=yn2]").val("-4");
        $("input[name=zn2]").val("0");

        $("input[name=xm1]").val("2");
        $("input[name=ym1]").val("0");
        $("input[name=zm1]").val("0");
        $("input[name=xm2]").val("0");
        $("input[name=ym2]").val("4");
        $("input[name=zm2]").val("0");

        $("input[name=xk1]").val("2");
        $("input[name=yk1]").val("0");
        $("input[name=zk1]").val("0");
        $("input[name=xk2]").val("0");
        $("input[name=yk2]").val("-4");
        $("input[name=zk2]").val("0");

        $("input[name=xj1]").val("-2");
        $("input[name=yj1]").val("0");
        $("input[name=zj1]").val("0");
        $("input[name=xj2]").val("2");
        $("input[name=yj2]").val("0");
        $("input[name=zj2]").val("0");


        renderOctahedron();

    });

});

/**
 * Отрисовка фигуры
 */
function renderOctahedron() {
    var x0 = 250;
    var y0 = 250;
    var z0 = 0;

    var za1 = z0 + parseInt($(".form-task input[name=za1]").val()) * 20;
    var xa1 = za1 + x0 + parseInt($(".form-task input[name=xa1]").val()) * 50;
    var ya1 = -za1 + y0 - parseInt($(".form-task input[name=ya1]").val()) * 50;
    var zb1 = z0 + parseInt($(".form-task input[name=zb1]").val()) * 20;
    var xb1 = zb1 + x0 + parseInt($(".form-task input[name=xb1]").val()) * 50;
    var yb1 = -zb1 + y0 - parseInt($(".form-task input[name=yb1]").val()) * 50;
    var zc1 = z0 + parseInt($(".form-task input[name=zc1]").val()) * 20;
    var xc1 = zc1 + x0 + parseInt($(".form-task input[name=xc1]").val()) * 50;
    var yc1 = -zc1 + y0 - parseInt($(".form-task input[name=yc1]").val()) * 50;
    var zd1 = z0 + parseInt($(".form-task input[name=zd1]").val()) * 20;
    var xd1 = zd1 + x0 + parseInt($(".form-task input[name=xd1]").val()) * 50;
    var yd1 = -zd1 + y0 - parseInt($(".form-task input[name=yd1]").val()) * 50;
    var za2 = -20 + z0 + parseInt($(".form-task input[name=za2]").val()) * 20;
    var xa2 = za2 + x0 + parseInt($(".form-task input[name=xa2]").val()) * 50 + 20;
    var ya2 = -za2 + y0 - parseInt($(".form-task input[name=ya2]").val()) * 50 - 20;
    var zb2 = -20 + z0 + parseInt($(".form-task input[name=zb2]").val()) * 20;
    var xb2 = zb2 + x0 + parseInt($(".form-task input[name=xb2]").val()) * 50 + 20;
    var yb2 = -zb2 + y0 - parseInt($(".form-task input[name=yb2]").val()) * 50 - 20;





    // Прямая L1L2
    var zL1 = z0 + parseInt($("input[name=zl1]").val()) * 20;
    var xL1 = zL1 + x0 + parseInt($("input[name=xl1]").val()) * 50;
    var yL1 = -zL1 + y0 - parseInt($("input[name=yl1]").val()) * 50;
    var zL2 = z0 + parseInt($("input[name=zl2]").val()) * 20;
    var xL2 = zL2 + x0 + parseInt($("input[name=xl2]").val()) * 50;
    var yL2 = -zL2 + y0 - parseInt($("input[name=yl2]").val()) * 50;
    // Прямая N1N2
    var zN1 = z0 + parseInt($("input[name=zn1]").val()) * 20;
    var xN1 = zN1 + x0 + parseInt($("input[name=xn1]").val()) * 50;
    var yN1 = -zN1 + y0 - parseInt($("input[name=yn1]").val()) * 50;
    var zN2 = z0 + parseInt($("input[name=zn2]").val()) * 20;
    var xN2 = zN2 + x0 + parseInt($("input[name=xn2]").val()) * 50;
    var yN2 = -zN2 + y0 - parseInt($("input[name=yn2]").val()) * 50;
    // Прямая M1M2
    var zM1 = z0 + parseInt($("input[name=zm1]").val()) * 20;
    var xM1 = zM1 + x0 + parseInt($("input[name=xm1]").val()) * 50;
    var yM1 = -zM1 + y0 - parseInt($("input[name=ym1]").val()) * 50;
    var zM2 = z0 + parseInt($("input[name=zm2]").val()) * 20;
    var xM2 = zM2 + x0 + parseInt($("input[name=xm2]").val()) * 50;
    var yM2 = -zM2 + y0 - parseInt($("input[name=ym2]").val()) * 50;
    // Прямая K1K2
    var zK1 = z0 + parseInt($("input[name=zk1]").val()) * 20;
    var xK1 = zK1 + x0 + parseInt($("input[name=xk1]").val()) * 50;
    var yK1 = -zK1 + y0 - parseInt($("input[name=yk1]").val()) * 50;
    var zK2 = z0 + parseInt($("input[name=zk2]").val()) * 20;
    var xK2 = zK2 + x0 + parseInt($("input[name=xk2]").val()) * 50;
    var yK2 = -zK2 + y0 - parseInt($("input[name=yk2]").val()) * 50;


    // Прямая J1J2
    var zJ1 = z0 + parseInt($("input[name=zj1]").val()) * 20;
    var xJ1 = zJ1 + x0 + parseInt($("input[name=xj1]").val()) * 50;
    var yJ1 = -zJ1 + y0 - parseInt($("input[name=yj1]").val()) * 50;
    var zJ2 = z0 + parseInt($("input[name=zj2]").val()) * 20;
    var xJ2 = zJ2 + x0 + parseInt($("input[name=xj2]").val()) * 50;
    var yJ2 = -zJ2 + y0 - parseInt($("input[name=yj2]").val()) * 50;



    var canvas = document.getElementById("canvas");
    if (canvas.getContext) {
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = "#00F";
        ctx.font = "12pt Arial";
        ctx.setLineDash([5, 3]);

        // Отрисовка направляющих
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.strokeStyle = "green";
        ctx.moveTo(x0, 0);
        ctx.lineTo(x0, 500);
        ctx.stroke();
        ctx.beginPath();
        ctx.strokeStyle = "red";
        ctx.moveTo(0, y0);
        ctx.lineTo(500, y0);
        ctx.stroke();
        ctx.beginPath();
        ctx.strokeStyle = "blue";
        ctx.moveTo(x0 - 500, y0 + 500);
        ctx.lineTo(x0 + 500, y0 - 500);
        ctx.stroke();
        ctx.setLineDash([]);

        // Отрисовка
        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.strokeStyle = "black";
        ctx.moveTo(xa1, ya1);
        ctx.lineTo(xb1, yb1);
        ctx.moveTo(xb1, yb1);
        ctx.lineTo(xc1, yc1);
        ctx.moveTo(xc1, yc1);
        ctx.lineTo(xd1, yd1);
        ctx.moveTo(xd1, yd1);
        ctx.lineTo(xa1, ya1);
        ctx.moveTo(xa1, ya1);
        ctx.lineTo(xa2, ya2);
        ctx.moveTo(xb1, yb1);
        ctx.lineTo(xa2, ya2);
        ctx.moveTo(xc1, yc1);
        ctx.lineTo(xa2, ya2);
        ctx.moveTo(xd1, yd1);
        ctx.lineTo(xa2, ya2);
        ctx.moveTo(xa1, ya1);
        ctx.lineTo(xb2, yb2);
        ctx.moveTo(xb1, yb1);
        ctx.lineTo(xb2, yb2);
        ctx.moveTo(xc1, yc1);
        ctx.lineTo(xb2, yb2);
        ctx.moveTo(xd1, yd1);
        ctx.lineTo(xb2, yb2);

        ctx.stroke();
        ctx.fillText("A1", xa1 - 20, ya1 - 5);
        ctx.fillText("B1", xb1 - 20, yb1 - 5);
        ctx.fillText("C1", xc1 - 20, yc1 - 5);
        ctx.fillText("D1", xd1 - 20, yd1 - 5);
        ctx.fillText("A2", xa2 - 20, ya2 - 5);
        ctx.fillText("B2", xb2 - 20, yb2 - 5);

        // Отрисовка прямых
        ctx.beginPath();
        ctx.lineWidth = 5;
        ctx.fillStyle = "red";
        ctx.strokeStyle = "red";
        // Прямая L1L2
        ctx.moveTo(xL1, yL1);
        ctx.lineTo(xL2, yL2);
        // Прямая N1N2
        ctx.moveTo(xN1, yN1);
        ctx.lineTo(xN2, yN2);
        // Прямая M1M2
        ctx.moveTo(xM1, yM1);
        ctx.lineTo(xM2, yM2);
        // Прямая K1K2
        ctx.moveTo(xK1, yK1);
        ctx.lineTo(xK2, yK2);
        // Прямая J1J2
        ctx.moveTo(xJ1, yJ1);
        ctx.lineTo(xJ2, yJ2);


        ctx.stroke();
        ctx.fillText("L1", xL1 + 5, yL1 + 20);
        ctx.fillText("L2", xL2 + 5, yL2 + 20);
        ctx.fillText("N3", xN1 + 5, yN1 + 20);
        ctx.fillText("N4", xN2 + 5, yN2 + 20);
        ctx.fillText("M1", xM1 + 5, yM1 + 20);
        ctx.fillText("M2", xM2 + 5, yM2 + 20);
        ctx.fillText("K1", xK1 + 5, yK1 + 20);
        ctx.fillText("K2", xK2 + 5, yK2 + 20);
        ctx.fillText("J1", xJ1 + 5, yJ1 + 20);
        ctx.fillText("J2", xJ2 + 5, yJ2 + 20);


    }
}
