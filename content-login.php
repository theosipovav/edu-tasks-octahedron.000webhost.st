<!-- Modal -->
<div class="modal fade" id="ModalLogIn" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="ModalLogInLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLogInLabel">Авторизация</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="FormLogIn" action="/" method="POST">
                    <div class="form-group">
                        <label for="InputLoginEmail">Адрес электронной почты</label>
                        <input type="email" name="email" class="form-control" id="InputLoginEmail" aria-describedby="emailHelp" required>
                        <small id="emailHelp" class="form-text text-muted">Мы никогда никому не передадим вашу электронную почту.</small>
                    </div>
                    <div class="form-group">
                        <label for="InputLoginPassword">Пароль</label>
                        <input type="password" name="password" class="form-control" id="InputLoginPassword" required>
                    </div>
                    <div class="form-group d-flex flex-column justify-content-center align-items-center mt-3">
                        <span>Нет аккаунта?</span>
                        <button type="button" class="btn btn-primary text-white link-of-login-in-reg">Регистрация</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Отмена</button>
                <button type="submit" form="FormLogIn" name="form-login" class="btn btn-primary text-white">Далее</button>
            </div>
        </div>
    </div>
</div>