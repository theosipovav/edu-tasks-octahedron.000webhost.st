<?php
global $messageSuccess;
global $messageWarning;
global $messageError;
?>
<div class="container">
    <nav class="navbar navbar-expand-lg d-flex flex-column">
        <a class="navbar-brand text-primary " href=".">
            <span class="inline-laptop">
                <i class="fab fa-adversal"></i>
                <span>Самосовмещенные многогранники <sub>октаэдр</sub></span>
            </span>
            <span class="inline-mobile">
                Самосовмещенные многогранники
            </span>
        </a>
        <div class="btn-group" role="group" aria-label="Basic example">
            <a href="/" type="button" class="btn btn-primary text-white">
                <span class="inline-laptop">Главная</span>
                <i class="fas fa-home inline-mobile"></i>
            </a>
            <a href="/?page=tasks" type="button" class="btn btn-primary text-white">
                <span class="inline-laptop">Задания</span>
                <i class="fas fa-list-ul inline-mobile"></i>
            </a>
            <a href="/?page=task-create" type="button" class="btn btn-primary text-white">
                <span class="inline-laptop">Добавить задание</span>
                <i class="fas fa-plus-square inline-mobile"></i>
            </a>
            <a href="/?page=answers" type="button" class="btn btn-primary text-white">
                <span class="inline-laptop">Результат</span>
                <i class="fas fa-tasks inline-mobile"></i>
            </a>
            <?php if ($isLogged) : ?>
                <button name="form-logout" form="FormPost" class="btn btn-primary text-white" title="Выход">
                    <span class="inline-laptop">Выход</span>
                    <i class="fas fa-sign-out-alt inline-mobile"></i>
                </button>
                <a href="/?page=profile" class="btn btn-primary text-white" title="Профиль">
                    <span class="inline-laptop">Профиль</span>
                    <i class="fas fa-user inline-mobile"></i>
                </a>
            <?php else : ?>
                <button class="btn btn-primary text-white" data-bs-toggle="modal" data-bs-target="#ModalLogIn" title="Вход">
                    <span class="inline-laptop">Вход</span>
                    <i class="fas fa-sign-in-alt inline-mobile"></i>
                </button>
                <button class="btn btn-primary text-white" data-bs-toggle="modal" data-bs-target="#ModalRegistration" title="Регистрация">
                    <span class="inline-laptop">Регистрация</span>
                    <i class="fas fa-user-plus inline-mobile"></i>
                </button>
            <?php endif ?>
        </div>
    </nav>
</div>
<?php
include_once 'content-login.php';
include_once 'content-registration.php';
?>
<div class="container">
    <?php if ($messageSuccess != "") : ?>
        <div class="card text-white bg-success mt-3 mb-3">
            <div class="card-header">Успешно!</div>
            <div class="card-body">
                <p class="card-text"><?= $messageSuccess ?></p>
            </div>
        </div>
    <?php endif ?>
    <?php if ($messageError != "") : ?>
        <div class="card text-white bg-danger mt-3 mb-3">
            <div class="card-header">Ошибка!</div>
            <div class="card-body">
                <p class="card-text"><?= $messageError ?></p>
            </div>
        </div>
    <?php endif ?>
    <?php if ($messageWarning != "") : ?>
        <div class="card text-white bg-warning mt-3 mb-3">
            <div class="card-header">Внимание!</div>
            <div class="card-body">
                <p class="card-text"><?= $messageWarning ?></p>
            </div>
        </div>
    <?php endif ?>
</div>
<form id="FormPost" action="/" method="post"></form>