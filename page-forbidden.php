<div class="row pt-3 pb-3">
    <div class="col-12 d-flex justify-content-center align-items-center">
        <h3 class="display-3 m-3 text-center">
            Доступ запрещен
        </h3>
    </div>
</div>
<div class="row">
    <div class="col-12 d-flex justify-content-center">
        <a href="/" class="btn btn-lg btn-primary text-white m-3 p-3">На главную</a>
    </div>
</div>