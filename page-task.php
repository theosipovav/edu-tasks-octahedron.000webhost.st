<?php
global $db;
$get = $_GET;
$task = $db->getTask($get['id']);


$answer = $db->getAnswerByUserAndTask($_SESSION['user']['id'], $task->id);
?>
<div class="row">
    <div class="col-12">
        <h2>Задание №<?= $task->id ?></h2>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        <p class="lead"><?= $task->text ?></p>
    </div>
</div>
<hr>



<form class="form-task">


    <div class="row mt-1">
        <div class="col-12 col-md-3 row">
            <div class="col-md-2">
                <h5>xA<sub>1</sub></h5>
            </div>
            <div class="col-md-10 d-flex">
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xa1" value="<?= $task->xa1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="ya1" value="<?= $task->ya1 ?>" class="form-control">
                    <label for="">y</label>

                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="za1" value="<?= $task->za1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 row">
            <div class="col-md-2">
                <h5>xB<sub>1</sub></h5>
            </div>
            <div class="col-md-10 d-flex">
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xb1" value="<?= $task->xb1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yb1" value="<?= $task->yb1 ?>" class="form-control">
                    <label for="">y</label>

                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zb1" value="<?= $task->zb1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 row">
            <div class="col-md-2">
                <h5>xC<sub>1</sub></h5>
            </div>
            <div class="col-md-10 d-flex">
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xc1" value="<?= $task->xc1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yc1" value="<?= $task->yc1 ?>" class="form-control">
                    <label for="">y</label>

                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zc1" value="<?= $task->zc1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 row">
            <div class="col-md-2">
                <h5>xC<sub>2</sub></h5>
            </div>
            <div class="col-md-10 d-flex">
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xc2" value="<?= $task->xc2 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yc2" value="<?= $task->yc2 ?>" class="form-control">
                    <label for="">y</label>

                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zc2" value="<?= $task->zc2 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 row">
            <div class="col-md-2">
                <h5>xD<sub>1</sub></h5>
            </div>
            <div class="col-md-10 d-flex">
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xd1" value="<?= $task->xd1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yd1" value="<?= $task->yd1 ?>" class="form-control">
                    <label for="">y</label>

                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zd1" value="<?= $task->zd1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 row">
            <div class="col-md-2">
                <h5>xA<sub>2</sub></h5>
            </div>
            <div class="col-md-10 d-flex">
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xa2" value="<?= $task->xa2 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="ya2" value="<?= $task->ya2 ?>" class="form-control">
                    <label for="">y</label>

                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="za2" value="<?= $task->za2 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 row">
            <div class="col-md-2">
                <h5>xB<sub>2</sub></h5>
            </div>
            <div class="col-md-10 d-flex">
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xb2" value="<?= $task->xb2 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yb2" value="<?= $task->yb2 ?>" class="form-control">
                    <label for="">y</label>

                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zb2" value="<?= $task->zb2 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>


    </div>
    <div class="row mt-3 mb-3">
        <div class="col-12 d-flex justify-content-center align-items-center">
            <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-3 d-flex flex-column">
            <h5>Прямая L1L2</h5>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>L1</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xl1" value="<?= $task->xl1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yl1" value="<?= $task->yl1 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zl1" value="<?= $task->zl1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>L2</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xl2" value="<?= $task->xl2 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yl2" value="<?= $task->yl2 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zl2" value="<?= $task->zl2 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 d-flex flex-column">
            <h5>Прямая N1N2</h5>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>N1</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xn1" value="<?= $task->xn1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yn1" value="<?= $task->yn1 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zn1" value="<?= $task->zn1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>N2</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xn2" value="<?= $task->xn2 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yn2" value="<?= $task->yn2 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zn2" value="<?= $task->zn2 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 d-flex flex-column">
            <h5>Прямая M1M2</h5>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>M1</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xm1" value="<?= $task->xm1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="ym1" value="<?= $task->ym1 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zm1" value="<?= $task->zm1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>M2</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xm2" value="<?= $task->xm2 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="ym2" value="<?= $task->ym2 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zm2" value="<?= $task->zm2 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 d-flex flex-column">
            <h5>Прямая K1K2</h5>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>K1</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xk1" value="<?= $task->xk1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yk1" value="<?= $task->yk1 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zk1" value="<?= $task->zk1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>K2</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xk2" value="<?= $task->xk2 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yk2" value="<?= $task->yk2 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zk2" value="<?= $task->zk2 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 d-flex flex-column">
            <h5>Прямая J1J2</h5>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>J1</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xj1" value="<?= $task->xj1 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yj1" value="<?= $task->yj1 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zj1" value="<?= $task->zj1 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
            <div class="d-flex">
                <p class="m-1 mr-3"><strong>J2</strong></p>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="xj2" value="<?= $task->xj2 ?>" class="form-control">
                    <label for="">x</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="yj2" value="<?= $task->yj2 ?>" class="form-control">
                    <label for="">y</label>
                </div>
                <div class="d-flex flex-column align-items-center">
                    <input type="number" name="zj2" value="<?= $task->zj2 ?>" class="form-control">
                    <label for="">z</label>
                </div>
            </div>
        </div>

    </div>


</form>

<hr>
<?php if ($answer == false) : ?>
    <form action="/?page=task&id=<?= $task->id ?>" id="FormAnswerCreate" method="post" class="row">
        <div class="col-md-8 d-flex">
            <label for="InputFormAnswerResult" class="mr-3" style="min-width: 100px;">Ваш ответ:</label>
            <input class="form-control flex-grow-1" type="text" name="res" id="InputFormAnswerResult">
            <input type="hidden" name="user" value="<?= $_SESSION['user']['id'] ?>">
            <input type="hidden" name="task" value="<?= $task->id ?>">
        </div>
        <div class="col-md-4 d-flex flex-column">
            <button type="submit" name="form-answer-create" class="btn btn-green text-white">Добавить</button>
        </div>
    </form>
<?php else : ?>
    <form action="/?page=task&id=<?= $task->id ?>" id="FormAnswerUpdate" method="post" class="row">
        <div class="col-md-8 d-flex">
            <label for="InputFormAnswerResult" class="mr-3" style="min-width: 100px;">Ваш ответ:</label>
            <input class="form-control flex-grow-1" type="text" name="res" id="InputFormAnswerResult" value="<?= $answer->res ?>">
            <input type="hidden" name="id" value="<?= $answer->id ?>">
        </div>
        <div class="col-md-4 d-flex flex-column">
            <button type="submit" name="form-answer-update" class="btn btn-green text-white">Обновить</button>
        </div>
    </form>
<?php endif ?>

<hr>
<div class="accordion accordion-flush" id="accordionFlushExample">
    <div class="accordion-item">
        <h2 class="accordion-header" id="flush-headingOne">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                Показать решение
            </button>
        </h2>
        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
            <div class="accordion-body">
                <?= $task->desc ?>
            </div>
        </div>
    </div>


</div>